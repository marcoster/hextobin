## Description
Simple hex-to-bin and bin-to-hex conversion routines
for C.

Uses small lookup-tables for fast operation.

## Usage:

```
uint8_t bin = 123;
uint8_t hex[2];

BinToHex(bin, hex);     // converts bin into hex
bin = HexToBin(hex);    // converts hex back to bin
```

## License
License information see 'LICENSE.txt' file.

