/**
 * @file        hexbin.h
 * @author      Marco Sterbik
 * @date        2015-06-16
 *
 * Copyright (C) 2015 Marco Sterbik
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 **/
#ifndef HEXBIN_H_
#define HEXBIN_H_

#include <stdint.h>

void BinToHex(uint8_t, uint8_t *);
uint8_t HexToBin(uint8_t *);

#endif


