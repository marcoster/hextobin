/**
 * @file        hexbin.h
 * @author      Marco Sterbik
 * @date        2015-06-16
 *
 * Copyright (C) 2015 Marco Sterbik
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 **/

#include "hexbin.h"

const uint8_t hexEncodingTable[] = {'0', '1', '2', '3',
                                    '4', '5', '6', '7',
                                    '8', '9', 'A', 'B',
                                    'C', 'D', 'E', 'F'};        ///< hex encoding table


const uint8_t hexDecodingTable[] = {0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 1, 2, 3, 4, 5, 6, 7, 
                                    8, 9, 0, 0, 0, 0, 0, 0, 
                                    0, 10, 11, 12, 13, 14, 15, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 10, 11, 12, 13, 14, 15, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0, 
                                    0, 0, 0, 0, 0, 0, 0, 0};    ///< hex decoding table


/**
 * @brief   Convert from byte to hex-characters
 *
 * @param   [b]     byte data to convert
 * @param   [*h]    pointer to store the hex-characters
 */
void BinToHex(uint8_t b, uint8_t *h)
{
    if(h == 0) {
        return;
    }

    *h++ = hexEncodingTable[(b >> 4) & 0x0F];
    *h = hexEncodingTable[b & 0x0F];
}


/**
 * @brief   Convert from hex-characters to byte
 *
 * @param   [*h]    pointer to the hex-characters (2 chars)
 *
 * @return          converted data as byte
 */
uint8_t HexToBin(uint8_t *h)
{
    return (hexDecodingTable[*h] << 4) + hexDecodingTable[*(h + 1)];
}


